import { defineNuxtConfig } from 'nuxt3';

require('dotenv').config();

const isSrr = process.env.SSR === '1';
const isProduction = process.env.NODE_ENV === 'production';

export default defineNuxtConfig({
    ssr: isSrr,
    target: isSrr ? 'server' : 'static',
    vite: false,
    env: {
        ENV: process.env.ENV,
        SSR: process.env.SSR,
        SITE_NAME: process.env.SITE_NAME,
    },
    css: [
        '@/assets/styles/css/variables.css',
        '@/assets/styles/css/antd.css',
        '@/assets/styles/css/style.css',
    ],
    buildModules: [
        '@nuxtjs/dotenv',
        '@intlify/nuxt3',
        '@nuxtjs/style-resources',
        [
            '@nuxtjs/google-fonts',
            {
                families: {
                    Roboto: [200, 300, 400, 500, 600, 700],
                    'Roboto Mono': true,
                    Lexend: [200, 300, 400, 500, 600, 700],
                },
            },
        ],
    ],
    intlify: {
        localeDir: 'locales',
        vueI18n: {
            locale: 'vi',
        },
    },
    build: {
        loaders: {
            cssModules: {
                modules: {
                    localIdentName: isProduction
                        ? '[hash:base64:4]'
                        : '[local]_[hash:base64:4]',
                },
            },
        },
    },
});
