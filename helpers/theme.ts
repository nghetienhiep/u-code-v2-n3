import { themeConstant } from '@/constants';
import { ICommonObject, EThemeMode } from '@/interfaces';
import colorHelper from './color';

const selectThemeColors = (
    colors: ICommonObject<string>,
    themeMode = EThemeMode.LIGHT,
) => {
    const {
        DEFAULT_RENDER_COLORS,
        DEFAULT_RENDER_OLD_COLORS,
        DEFAULT_THEME_COLORS,
    } = themeConstant;
    const themeColors = DEFAULT_THEME_COLORS[themeMode];

    return DEFAULT_RENDER_COLORS.reduce(
        (result: ICommonObject<string>, colorName: string) => {
            const colorOldName = DEFAULT_RENDER_OLD_COLORS[colorName];
            const colorNewName = `--${colorName}`;
            result[colorNewName] =
                colors[colorNewName] ||
                (colorOldName && colors[`--${colorOldName}`]) ||
                themeColors[colorNewName];
            return result;
        },
        {},
    );
};

const getThemeColors = (
    colors: ICommonObject<string>,
    themeMode = EThemeMode.LIGHT,
) => {
    const colorsSelected = selectThemeColors(colors, themeMode);
    const primaryColor = colorsSelected['--primary'];
    const primaryColors = (
        primaryColor ? colorHelper.getAntdColors(primaryColor, themeMode) : []
    ).reduce((result: ICommonObject<string>, color: string, index: number) => {
        result[`--primary-${index + 1}`] = color;
        return result;
    }, {});
    const el = document && document.documentElement.style;
    Object.entries({
        ...colorsSelected,
        ...primaryColors,
    }).forEach(([key, color]) => {
        if (color) {
            el.setProperty(key, color as string);
            const rgb = colorHelper.toNum3(color as string);
            el.setProperty(`${key}-rgb`, rgb.join(','));
        }
    });
};

export default {
    selectThemeColors,
    getThemeColors,
};
