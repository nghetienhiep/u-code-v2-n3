import { EThemeMode } from '@/interfaces';

const { generate } = require('@ant-design/colors');

const getAntdColors = (color: string, mode: EThemeMode) => {
    const options =
        mode === EThemeMode.DARK ? { theme: EThemeMode.DARK } : undefined;
    return generate(color, options);
};

const isHex = (color: string) => {
    return color.length >= 4 && color[0] === '#';
};

const isRgb = (color: string) => {
    return color.length >= 10 && color.slice(0, 3) === 'rgb';
};

const isRgba = (color: string) => {
    return color.length >= 13 && color.slice(0, 4) === 'rgba';
};

const hexToRgb = (hex: string) => {
    return (
        hex
            .replace(
                /^#?([a-f\d])([a-f\d])([a-f\d])$/i,
                (m, r, g, b) => '#' + r + r + g + g + b + b,
            )
            .substring(1)
            .match(/.{2}/g) || []
    ).map(x => parseInt(x, 16));
};

const toNum3 = (color: string) => {
    if (isHex(color)) {
        return hexToRgb(color);
    }
    let colorStr = '';
    if (isRgb(color)) {
        colorStr = color.slice(5, color.length);
    } else if (isRgba(color)) {
        colorStr = color.slice(6, color.lastIndexOf(','));
    }
    const rgb = colorStr.split(',');
    const r = parseInt(rgb[0]);
    const g = parseInt(rgb[1]);
    const b = parseInt(rgb[2]);
    return [r, g, b];
};

export default {
    isHex,
    isRgb,
    isRgba,
    toNum3,
    hexToRgb,
    getAntdColors,
};
