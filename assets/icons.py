import os

with open("icons.js", "w") as a:
    a.write('export const icons = [')
    for path, subdirs, files in os.walk(r'icons'):
        for filename in files:
            name = str(os.path.splitext(filename)[0])
            a.write("'" + name  + "',")
    a.write('];')
