export const icons = [
    '3-dot-line',
    'app',
    'arrow-bottom-line',
    'arrow-down-line',
    'arrow-left-line',
    'arrow-right-line',
    'arrow-top-line',
    'arrow-up-line',
    'board-line',
    'camera-line',
    'check-list-three-line',
    'check-list-two-line',
    'clock-circle-line',
    'clock-fill',
    'close-circle-fill',
    'close-line',
    'code-rectangle-line',
    'course-line',
    'cup-line',
    'desktop-line',
    'file-line',
    'filter-line',
    'google',
    'home-line',
    'loading',
    'lock-line',
    'logout-line',
    'minus-line',
    'moon-line',
    'page-line',
    'percent-line',
    'plus-line',
    'question-circle-line',
    'question-line',
    'search-line',
    'setting-line',
    'sort-arrow-line',
    'sort-line',
    'sun-line',
    'unlock-line',
    'user-line',
    'user-table-line',
    'video-circle-line',
    'video-square-fill',
    'video-square-line',
    'collapse-line',
    'expand-line',
];
