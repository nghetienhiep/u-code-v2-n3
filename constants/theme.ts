import { EThemeMode, ICommonObject } from '@/interfaces';

const DEFAULT_RENDER_OLD_COLORS: ICommonObject<string> = {
    'component-background': 'block-background',
};

const DEFAULT_RENDER_COLORS: string[] = [
    'white',
    'black',
    'primary',
    'danger',
    'warning',
    'body-background',
    'component-background',
];

const DEFAULT_COMMON_COLORS: ICommonObject<string> = {
    '--white': '#fff',
    '--black': '#000',
};

const DEFAULT_LIGHT_THEME_COLORS: ICommonObject<string> = {
    '--primary': '#1caa4d',
    '--danger': '#F33620',
    '--warning': '#FF9900',
    '--body-background': '#F0F2F5',
    '--body-background-inverse': '#1E1E2F',
    '--component-background': '#FFFFFF',
    '--component-background-inverse': '#282A36',
    ...DEFAULT_COMMON_COLORS,
};

const DEFAULT_DARK_THEME_COLORS: ICommonObject<string> = {
    '--primary': '#1caa4d',
    '--danger': '#F33620',
    '--warning': '#FF9900',
    '--body-background': '#1E1E2F',
    '--body-background-inverse': '#F0F2F5',
    '--component-background': '#282A36',
    '--component-background-inverse': '#FFFFFF',
    ...DEFAULT_COMMON_COLORS,
};

const DEFAULT_THEME_COLORS: ICommonObject<ICommonObject> = {
    [EThemeMode.LIGHT]: DEFAULT_LIGHT_THEME_COLORS,
    [EThemeMode.DARK]: DEFAULT_DARK_THEME_COLORS,
};

export default {
    DEFAULT_THEME_COLORS,
    DEFAULT_RENDER_COLORS,
    DEFAULT_RENDER_OLD_COLORS,
    DEFAULT_DARK_THEME_COLORS,
    DEFAULT_LIGHT_THEME_COLORS,
};
