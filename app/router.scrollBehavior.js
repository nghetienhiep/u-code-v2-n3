export default function (to, _, savedPosition) {
    if (savedPosition) {
        return savedPosition;
    } else {
        let position = {};
        if (to.matched.length < 2) {
            position = window.scrollTo({ top: 0, behavior: 'smooth' });
        } else if (
            to.matched.some(r => r.components.default.options.scrollToTop)
        ) {
            position = window.scrollTo({ top: 0, behavior: 'smooth' });
        }
        if (to.hash) {
            position = window.scrollTo({
                top:
                    document.querySelector(to.hash).offsetTop +
                    window.innerHeight,
                behavior: 'smooth',
            });
        }
        return position;
    }
}
