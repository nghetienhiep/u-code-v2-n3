export * from './user';
export * from './tenant';

export type OptionalPick<T, K extends keyof T> = Pick<Partial<T>, K>;

export type Optional<T, K extends keyof T> = OptionalPick<T, K> & Omit<T, K>;

export type CustomErrors = {
    errors: {
        errorName: string[];
    };
};

export interface ICommonObject<T = any> {
    [key: string]: T;
}

export interface IResponse<T = any> {
    state: boolean;
    data: T | null | undefined;
    code: number;
    message?: string;
    metadata: any | null | undefined;
}

export type IResponsePromise<T> = Promise<IResponse<T>>;

export enum EThemeShape {
    ROUND = 'round',
    SQUARE = 'square',
}

export enum EThemeMode {
    LIGHT = 'light',
    DARK = 'dark',
}

export enum EThemeMenuPosition {
    LEFT = 'left',
    CENTER = 'center',
}

export type ITheme = {
    shape: EThemeShape;
    mode: EThemeMode;
    menuPosition: EThemeMenuPosition;
};

export enum ELocale {
    VI = 'vi',
    EN = 'en',
}

export enum EDevice {
    MOBILE_SM = 'mobile_small',
    MOBILE = 'mobile',
    TABLET = 'tablet',
    DESKTOP = 'desktop',
    DESKTOP_LG = 'desktop_large',
}

export type IConfig = {
    device: EDevice;
    locale: ELocale;
    locales: ELocale[];
    localeImage: { [key: string]: any };
};
