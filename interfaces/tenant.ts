import { ICommonObject } from '@/types/index';

interface ITenantDomain {
    id: number;
    name: string;
    allow_to_delete: boolean;
}

export interface ITenantResponse {
    id: number;
    type: number;
    favicon: string;
    name: string;
    description: string;
    social_media: string;
    locales: string;
    slogan: string;
    logo: string;
    background: string;
    options: string;
    appearance_configuration: string;
    domains: ITenantDomain[];
}

export interface IAppearanceConfigurationColor {
    light: { [key: string]: string };
    dark: { [key: string]: string };
}

export interface IAppearanceConfigurationLanguage {
    state: boolean;
    is_default: boolean;
}

export interface IAppearanceConfiguration {
    colors: IAppearanceConfigurationColor;
    languages: { [key: string]: IAppearanceConfigurationLanguage };
}

export type ITenantSocialMedia = ICommonObject;
export type ITenantDescription = ICommonObject;
export type ITenantSlogan = ICommonObject;
export type ITenantOptions = {
    header: ICommonObject;
    home: ICommonObject;
    footer: ICommonObject;
};

export interface ITenant {
    id: number;
    type: number;
    favicon: string;
    name: string;
    description: ITenantDescription;
    social_media: ITenantSocialMedia;
    locales: string;
    slogan: ITenantSlogan;
    logo: string;
    background: string;
    options: ITenantOptions;
    appearance_configuration: IAppearanceConfiguration;
    domains: ITenantDomain[];
}
